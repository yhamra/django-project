from django.contrib import admin

from .models import Iso3Country
# Register your models here.
admin.site.register(Iso3Country)