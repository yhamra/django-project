from django.apps import AppConfig


class Iso3CountryConfig(AppConfig):
    name = 'iso3country'
