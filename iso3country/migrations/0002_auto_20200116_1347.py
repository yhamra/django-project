# Generated by Django 2.2.9 on 2020-01-16 12:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('iso3country', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='countryiso3',
            table='iso3country_countryiso3',
        ),
    ]
