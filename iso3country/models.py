from django.db import models

# Create your models here.
class Iso3Country(models.Model):
    iso3 = models.CharField(primary_key=True, max_length=6)
    pays = models.CharField(max_length=50, blank=True, null=True)
    source_plateforme = models.CharField(max_length=30, blank=True, null=True)  # Field name made lowercase.
    source_actor = models.CharField(max_length=30, blank=True, null=True)  # Field name made lowercase.
    source_date = models.CharField(max_length=30, blank=True, null=True)  # Field name made lowercase.
    wyzy_date_get = models.CharField(max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'iso3country'

