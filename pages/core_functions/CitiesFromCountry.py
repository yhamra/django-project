import csv
import logging
import os
from datetime import datetime as dt

from django.conf import settings

class Country:
    BASE_DIR = settings.BASE_DIR
    __world_cities_namefile__ = os.path.join(BASE_DIR,'pages', 'core_functions', 'worldcities.csv')
    __iso3_country_namefile__ = os.path.join(BASE_DIR,'pages', 'core_functions', 'iso3_country.csv')
    code_iso3 = None
    cities = None

    # Create a custom logger
    logger = logging.getLogger(__name__)

    # Create handlers
    c_handler = logging.StreamHandler()
    f_handler = logging.FileHandler('file.log')
    c_handler.setLevel(logging.WARNING)
    f_handler.setLevel(logging.ERROR)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(name)s - [%(levelname)s] - %(message)s')
    f_format = logging.Formatter('%(asctime)s - %(name)s -[%(levelname)s] - %(message)s')
    c_handler.setFormatter(c_format)
    f_handler.setFormatter(f_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)

    def __init__(self, country_name_ascii):
        super().__init__()
        with open(self.__iso3_country_namefile__, "r", encoding="utf-8") as f:
            iso3_country = csv.reader(f, delimiter=",")
            try:
                self.code_iso3 = self.__get_iso3_from_country_name__(iso3_country, country_name_ascii)
            except Exception as exc:
                self.logger.exception(f"Problème lors de l'obtention du code ISO3 pour {country_name_ascii}.", exc_info=False)

        with open(self.__world_cities_namefile__, "r", encoding="utf-8") as f:
            world_cities = csv.reader(f, delimiter=",")
            try:
                self.cities = self.__get_cities_from_iso3__(world_cities, self.code_iso3)
            except Exception as exc:
                self.logger.exception(f"Problème lors de l'obtention des villes pour {self.code_iso3}.", exc_info=False)

    
    def __get_iso3_from_country_name__(self, csv_reader, country_name):
        country_name = country_name.replace("’","'") ## Remplace les apostrophes anglaises par les apostrophes francaises
        for row in csv_reader:
            if country_name.lower() == row[1].lower():
                return row[0]
        raise Exception("Le pays n'existe pas ou n'est pas reconnu. Il s'agit peut-être d'une région ou d'une ville.")

    def __get_cities_from_iso3__(self, csv_reader, code_iso3):
        c = 0
        liste_ville = []
        for row in csv_reader:
            if c == 3:
                return liste_ville
            elif code_iso3 == row[6]:
                dict_city = {'city_ascii': row[1], 'id': int(row[10]), 'lat': row[2], 'lng': row[3], 'population': row[9] }
                liste_ville.append(dict_city)
                c+=1
        raise Exception("Le code ISO3 n'existe pas ou n'est pas reconnu.")
