import requests
from datetime import datetime as dt

def get_flights(lat_from, lng_from, radius_from, lat_to, lng_to, radius_to, month):
    api_key = "vOPo9tDwTnZGhB5nT8evvhqLvGS1H9ZE"
    headers = {'apikey': api_key}
    
    now = dt.now()

    if abs(now.month - int(month)) >= 7:
        return []
    
    if len(month) == 1:
        month = "0"+month

   
    
    if now.month > int(month) and now.month != 1:
        year = now.year + 1
    else :
        year = now.year
    

    url = f"https://kiwicom-prod.apigee.net/v2/search?fly_from={lat_from}-{lng_from}-{radius_from}&fly_to={lat_to}-{lng_to}-{radius_to}&dateFrom=10/{month}/{year}&dateTo=20/{month}/{year}"
    
    r = requests.get(url, headers=headers).json()
    data = r['data']
   

    def get_stopovers(lst_stopovers):
        for stop in lst_stopovers:
            res = " - " + stop
        return res

    city_from = data[0]['cityFrom']
    list_flights = []
    for flight in data:
        price = flight['price']
        local_departure = flight['local_departure']
        local_arrival = flight['local_arrival']
        stopover_bool = False

        route = flight['route']
        if len(route) >= 2:
            stopover_bool = True
            stopovers = []
            for i in range(len(route)-1):
                stopovers.append(route[i]['cityTo'])
            city_to = route[-1]['cityTo']
        else:
            city_to = route[0]['cityTo']
        
        stopover_str = "Pas d'escale" if not(stopover_bool) else get_stopovers(stopovers)

        flight = f"""
        {city_from} ---> {city_to}
        Prix : {price}
        Escale : {stopover_str}
        Heures de départ et arrivée (locales) : {local_departure} ---> {local_arrival}
        """
        list_flights.append(flight)
    return list_flights

month_travel = "7"

## Casablanca
lat_from = "33.6000"
lng_from = "-7.6164"
radius_from = "50km" # in km

## Paris
lat_to = "48.8667"
lng_to = "2.3333"
radius_to = "50km" # in km

flights = get_flights(lat_from, lng_from, radius_from, lat_to, lng_to, radius_to, month_travel)

if flights == []:
    print("Pas de vol disponible")
else:
    for flight in flights:
        print(flight)
    print(flights[0])