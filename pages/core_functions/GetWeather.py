import requests
import json
from datetime import datetime as dt
import sys
import logging

# from CitiesFromCountry import Country
from pages.core_functions.CitiesFromCountry import Country

# Create a custom logger
logger = logging.getLogger(__name__)

# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('file.log')
c_handler.setLevel(logging.WARNING)
f_handler.setLevel(logging.ERROR)

# Create formatters and add it to handlers
c_format = logging.Formatter('%(name)s - [%(levelname)s] - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(name)s -[%(levelname)s] - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)

########################################################################################################################
api_key = "54d15889367a44c188684059ca34f90f"

def get_historical_weather_from_cityname(lat, long, month):    
    last_year = dt.now().year - 1
    if len(month) == 1:
        month = "0"+month

    # Par défaut, on se réfère au 15 du mois à 12h de l'année précédente
    date = f"{last_year}-{month}-15T12:00:00"
    #https://api.darksky.net/forecast/[key]/[latitude],[longitude],[time]
    url = f"https://api.darksky.net/forecast/{api_key}/{lat},{long},{date}?units=si&exclude=currently,flags,hourly,minutely"

    json_response = requests.get(url).json()
    return json_response

def get_current_weather_from_cityname(lat, long):
    now = dt.now().strftime("%Y-%m-%dT%H:%M:%S")
    #https://api.darksky.net/forecast/[key]/[latitude],[longitude],[time]
    url = f"https://api.darksky.net/forecast/{api_key}/{lat},{long},{now}?units=si&exclude=daily,flags,hourly,minutely"

    json_response = requests.get(url).json()
    return json_response

if __name__ == '__main__':
    country_name = "pologne"
    country= CitiesFromCountry.Country(country_name)
    month = "8"

    if country.cities is None:
        logger.error("Pas de villes disponibles.")
        sys.exit()
    
    for city in country.cities:
        print("-------------------------------------------")
        lat = city['lat']
        lng = city['lng']
        month_now = dt.now().month
        
        if int(month) == month_now: #Si le mois choisi est le mois courant
            try:
                weather_city = get_current_weather_from_cityname(lat, lng)
            except Exception as exc:
                logger.exception("Problème lors de l'appel de l'API météo", exc_info=False)
                continue
        else:
            try:
                weather_city = get_historical_weather_from_cityname(lat, lng, month)
            except Exception as exc:
                logger.exception("Problème lors de l'appel de l'API météo", exc_info=False)
                continue
        print(city['city_ascii'])
        print(weather_city)