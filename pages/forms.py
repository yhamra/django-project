from django import forms

class SuggestionsForm(forms.Form):
    MONTHS =['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre']
    MONTH_CHOICES = [('', '-- Choisissez un mois --'), ] + [(month, month) for month in MONTHS]
    TYPES = ['plage et soleil', 'ski, neige et montagne', 'nature, randonnées et visites']
    TYPE_CHOICES = [(t, t) for t in TYPES]
    
    
    month = forms.ChoiceField(choices=MONTH_CHOICES, widget=forms.Select(attrs={'onchange':'get_travel_suggestions();'}))
    type = forms.ChoiceField(choices=COTYPE_CHOICESLOR_CHOICES)