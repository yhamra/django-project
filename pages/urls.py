from django.conf.urls import url
from django.urls import path
from .views import suggest_view, index_view, country_info_view


urlpatterns = [

        path('', index_view, name='index'), 
        path('suggest/', suggest_view, name='suggest'), 
        path('country_info/', country_info_view, name='country_info')
]