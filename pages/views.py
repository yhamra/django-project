from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from django.conf import settings

from pages.core_functions.CitiesFromCountry import Country
from pages.core_functions.GetWeather import get_current_weather_from_cityname
from pages.core_functions.GetWeather import get_historical_weather_from_cityname

import json
import os
import sys
from datetime import datetime as dt
import logging



################################################## VIEWS

def index_view(request, *args, **kwargs):
    return render(request, "index.html", {})


def suggest_view(request):
    if request.method == 'GET' and request.is_ajax():
        travel_month = request.GET['travel_month']
        travel_type = request.GET['travel_type']
        
        #Get travel data
        BASE_DIR = settings.BASE_DIR
        file_path = os.path.join(BASE_DIR, 'pages', 'data', 'ouwekan_data.json')
        with open(file_path, encoding='utf-8') as json_file:
            travel_data = json.load(json_file)
        suggestions = travel_data['yes'][travel_month][travel_type]

        # Continent and Countries list / weather
        list_continent_countries = []
        list_countries_weather = []
        for continent, countries in suggestions.items():
            if countries == {}:
                continue
            list_continent_countries.append({'continent': continent, 'countries': countries})
            
            # for c in countries:
            #     weather = get_weather(c, travel_month)
            #     list_countries_weather.append({c : weather})

        data = {}
        data['suggestions'] = list_continent_countries
        # data['weather_by_country'] = list_countries_weather

        return JsonResponse(data)
    else:
        return HttpResponse("unsuccesful")


def country_info_view(request):
    # if request.method == 'GET':
        # selected_country = request.GET['selected_country'].replace('_',' ')
        # selected_month   = request.GET['selected_month']
        
    return HttpResponse("success")
    # else:
    #     return HttpResponse("unsuccesful")






################################################## FONCTIONS ANNEXES


# Create a custom logger
logger = logging.getLogger(__name__)

# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('file.log')
c_handler.setLevel(logging.WARNING)
f_handler.setLevel(logging.ERROR)

# Create formatters and add it to handlers
c_format = logging.Formatter('%(name)s - [%(levelname)s] - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(name)s -[%(levelname)s] - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)

def get_weather(selected_country, selected_month):
    country = Country(selected_country)

    if country.cities is None:
        logger.error("Pas de villes disponibles.")
        return
    
    data = {}
    data['country'] = selected_country
    data['cities_info'] = []

    dict_weather_cities=[]
    for city in country.cities:
        lat = city['lat']
        lng = city['lng']
        month_now = dt.now().month
        if int(selected_month) == month_now: #Si le mois choisi est le mois courant
            try:
                weather_city = get_current_weather_from_cityname(lat, lng)
            except Exception as exc:
                logger.exception("Problème lors de l'appel de l'API météo", exc_info=False)
                weather_city = "Pas de données météo disponibles"
                continue
        else:
            try:
                weather_city = get_historical_weather_from_cityname(lat, lng, selected_month)
            except Exception as exc:
                logger.exception("Problème lors de l'appel de l'API météo", exc_info=False)
                weather_city = "Pas de données météo disponibles"
                continue
        weather_city['cityname']= city['city_ascii']
        dict_weather_cities.append(weather_city)
    return dict_weather_cities


    