


function country_block(country) {
    
    html = `
            
                <div class="col-lg-3 intro_col ${country.replace(/\s/g, '_')}">
                   
                        <div class="intro_item see_more">
                            <div class="intro_item_over_info">    
                            </div>
                            <div class="intro_item_overlay"></div>
                            <!-- Image by https://unsplash.com/@willianjusten -->
                            <div class="intro_item_background"
                                style="background-image:url('/static/images/intro_1.jpg')">
                            </div>
                            <div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
                                <div class="button intro_button">
                                    <div class="button_bcg">
                                    </div>
                                    
                                </div>
                                <div class="intro_center text-center">
                                    <h1>${country}</h1>
                                </div>
                            </div>
                        </div>
                    
                </div>
                

    `;  
    return html;
  }






$(".suggest").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(".intro .container").empty()

    var month;
    var type;
    month = $("#month_choice option:selected").val();
    type = $("#trip_type option:selected").val();
    $.ajax(
            {
            type: "GET",
            url: "suggest",
            data: {
                travel_month: month,
                travel_type: type
            },
            success: function (data) {              
                var suggestions = data['suggestions']
                console.log(suggestions)
                // var weather = data['weather_by_country']
                
                //Displaying countries by continent
                if (!suggestions.length) {
                    $(".intro .container").html("Pas de suggestions");
                } else {
                    for(var i = 0; i < suggestions.length; i++) {
                        continent = suggestions[i].continent
                        continent_block = ` <div class="continent_block"><h1 class ="continent_name">${continent}</h1></div>
                        <div class="row intro_items ${continent.replace(/\s/g, '_')}">
                        </div>
                        `
                    
                        $(".intro .container").append(continent_block)
                        for(var j = 0; j < suggestions[i].countries.length; j++) {
                            var country = suggestions[i].countries[j];
                            var res = country_block(country);

                            $("."+continent.replace(/\s/g, '_')).append(res);
                            
                            // console.log(weather)
                            // if (weather[country] !== undefined){
                            //     temperature = weather[country].daily.data.temperatureMax
                            //     if ('daily' in  weather[country]) {
                            //         $(".weather_"+country.replace(/\s/g, '_')).html(temperature.split('.')[0])
                            //     } else {
                            //         $(".weather_"+country.replace(/\s/g, '_')).html(temperature.split('.')[0])
                            //     }
                            // } else {
                            //     $(".weather_"+country.replace(/\s/g, '_')).html("Météo non disponible")
                            // }
                        }    
                    }

                    }
    
            },
            error: function(){
                console.log("ca marche pas")
                alert("Une erreur est survenue. Veuillez réessayer dans quelques instants.")
             }

        })
});

