from django.apps import AppConfig


class WorldcitiesConfig(AppConfig):
    name = 'worldcities'
