from django.db import models

# Create your models here.
class City(models.Model):
    city = models.CharField(max_length=50, blank=True, null=True)
    city_ascii = models.CharField(max_length=50, blank=True, null=True)
    lat = models.CharField(max_length=10, blank=True, null=True)
    lng = models.CharField(max_length=10, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    iso2 = models.CharField(max_length=2, blank=True, null=True)
    iso3 = models.CharField(max_length=3, blank=True, null=True)
    admin_name = models.CharField(max_length=100, blank=True, null=True)
    capital = models.CharField(max_length=25, blank=True, null=True)
    population = models.FloatField(blank=True, null=True)
    id_city = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'city'